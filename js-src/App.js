'use strict';

var App = function() {
  let exports;

  const API_URL     = 'https://hacker-news.firebaseio.com/v0/',
        BLOCK_COUNT = 30;

  let apiHandler,
      overviewView,
      detailView;

  let main,
      commentContainer,

      loadMoreButton;

  //constructor
  function init() {

    apiHandler = new ApiHandler(API_URL);

    main = document.querySelector('main');

    if(typeof config !== 'undefined' && config.type === 'overview') {
      //initialise template renderer
      overviewView = new OverviewView(main, config.baseURL + 'view/');

      //get load more button
      loadMoreButton = main.querySelector('.load-more');

      //get items of top stories
      apiHandler.getIDs(config.index)
        .then(itemCount => {
          loadStoryBlock(itemCount);

          //add click listener to load more items
          loadMoreButton.addEventListener('click',(event) => {
            event.preventDefault();
            //show loader
            main.classList.add('loading');
            //load next block
            loadStoryBlock(itemCount);
          });
        })
        .catch(error => console.log(error));

    } else if(typeof config !== 'undefined' && config.type === 'detail') {
      detailView = new DetailView(main);

      //get item detail data
      apiHandler.getItem(config.id)
        .then(data => {
          //display item
          detailView.render([data]);

          //if there are comments, load them
          if(typeof data.commentIDs !== 'undefined' && data.commentIDs.length > 0) {
            loadComments(data.commentIDs);
          }
        })
        .catch(error => {
          console.log(error);

          //send to 404 page
          if(config.notFoundURL) {
            window.location.replace(config.notFoundURL);
          }
        });
    }
  }

  //try to load data by block
  function loadStoryBlock(maxIndex) {
    let length = Math.min(BLOCK_COUNT, maxIndex);

    let itemPromises = [];
    
    for (let i = length; i--;) {
      let entry = apiHandler.nextOverviewItem();
      itemPromises.push( entry.value );

      //hide more button if there are no addional items to load
      if(entry.done) {
        main.classList.add('hide-more');
      }
    }
    
    Promise.all(itemPromises)
      .then(values  => {

        overviewView.render(values);

        //place load more button as last child again
        main.appendChild(loadMoreButton);

        //remove loader
        main.classList.remove('loading');
      });
  }

  //load comments in detail view
  function loadComments(commentIDs) {
    apiHandler.getRelatedData(commentIDs)
      .then(values  => {
        //create container for comments
        commentContainer = document.createElement('section');
        commentContainer.classList.add('comments');

        //handle rendering of data
        detailView.render(values, commentContainer);

        main.appendChild(commentContainer);

        //add close listener for possibility to close/open comments
        addCloseHandler(commentContainer.querySelectorAll('.comment'));

        main.classList.remove('loading');
      });
  }

  //add click listener which toggles closed class
  function addCloseHandler(nodelist) {
    for(let i = 0; i < nodelist.length; i++) {
      const closeButton = nodelist[i].querySelector('.close');

      if(closeButton) {
        closeButton.addEventListener('click',(event) => {
          nodelist[i].classList.toggle('closed');
        });
      }
    }
  }

  init();
  return exports;
}();