'use strict';

var DetailView = function(parent, detailURL) {
  let exports = {};

  /**
   * renders HTML content of DataItem for detail pages
   * @param  {DataEntryItem|CommentItem} [data] data to render
   * @param  {DOMNode} [container = parent] container the rendered HTML should be added to
   */
  exports.render = function(data, container = parent) {
    let fragment = document.createDocumentFragment();

    for(const dataItem of data) {
      //check if there is valid data and if it should be displayed
      if(typeof dataItem === 'undefined' || !(dataItem instanceof DataItem) || !dataItem.visible) continue;

      if(dataItem.type === 'comment') {
        fragment.appendChild(renderComment(dataItem));
      } else if(dataItem instanceof DataEntryItem) {
        fragment.appendChild(renderItem(dataItem));
      }
    }

    container.appendChild(fragment);
  };

  function renderItem(data) {
    let article = document.createElement('article');
    article.classList.add('item');
    article.classList.add(data.type);
    article.setAttribute('data-item-id',data.id);

    article.innerHTML = `
      <div class="left">
        <span class="points">${data.score}</span>
      </div>
      <div class="center">
        <div class="meta">Posted by <span class="user">${data.user}</span> <time datetime="${data.date}">${data.timeAgo}</time></div>

        <h2 class="title">${getTitleTemplate(data)}</a></h2>
        ${getRelatedTemplate(data)}
        <div class="comment-count"><span class="count">${(!isNaN(data.commentCount)) ? data.commentCount : 0}</span></div>
      </div>
      <div class="right">
        <a class="vote" href=""></a>
      </div>`;

    return article;
  }

  function getTitleTemplate(data) {
    if(data.url) {
      return `<a href="${data.url}">${data.title}</a>`;
    } else {
      return `${data.title}`;
    }
  }

  function getRelatedTemplate(data) {
    if(data.type === 'poll' && data.hasParts) {
      const highestScore = data.highestPartsScore;
      
      return `
      <ul class="poll-parts">
        ${data.parts.map(part => {
          //get relative score compaired to other options as number between 0 and 100
          const relativeScore = Math.max(0,Math.min(100,Math.round((part.score / highestScore) * 100)));

          return `
          <li class="poll-part">
            <h3>${part.text}</h3><span style="width:${relativeScore}%" data-score="${part.score}"></span>
          </li>`;
        }).join('')}
      </ul>`;
    } else {
      return ``;
    }
  }

  function renderComment(data) {
    let container = document.createElement('div');
    container.classList.add(data.type);
    container.setAttribute('data-item-id',data.id);

    container.innerHTML = `
      <a class="close"></a>
      <div class="comment-body">
        <div class="meta" data-comment-count="(${data.commentChildCount} comments)"><span class="user">${data.user}</span> - <time datetime="${data.date}">${data.timeAgo}</time></div>

        <div class="text">${data.text}</div>
      </div>`;

    if(data.hasChildren) {
      let childContainer = document.createElement('div');
      childContainer.classList.add('child-item');
      childContainer.setAttribute('item-id',data.id);

      exports.render(data.commentChildren, childContainer);

      container.appendChild(childContainer);
    }

    return container;
  }

  return exports;
};