'use strict';

var OverviewView = function(parent, detailURL) {
  let exports = {};

  /**
   * renders html content of DataEntryItem for overview pages
   * adds html as child to parent
   * @param  {DataEntryItem} [data] data to render
   */
  exports.render = function(data) {
    let fragment = document.createDocumentFragment();

    for(const dataItem of data) {
      //check if there is valid data and if it should be displayed
      if(typeof dataItem === 'undefined' || !(dataItem instanceof DataEntryItem) || !dataItem.visible) continue;

      fragment.appendChild(renderItem(dataItem));
    }

    parent.appendChild(fragment);
  };

  function renderItem(data) {
    let article = document.createElement('article');
    article.classList.add('item');
    article.classList.add(data.type);
    article.setAttribute('data-item-id',data.id);

    article.innerHTML = `
      <div class="left">
        <span class="points">${data.score}</span>
      </div>
      <div class="center">
        <div class="meta">Posted by <span class="user">${data.user}</span> <time datetime="${data.date}">${data.timeAgo}</time></div>

        <h2 class="title">${getTitleTemplate(data)}</h2>
        <a class="related" href=""></a>
        
        <div class="comment-count"><a href="${detailURL}${data.id}/"><span class="count">${(!isNaN(data.commentCount)) ? data.commentCount : 0}</span></a></div>
      </div>
      <div class="right">
        <a class="vote" href=""></a>
      </div>`;

    return article;
  }

  function getTitleTemplate(data) {
    if(data.url) {
      return `<a href="${data.url}">${data.title}</a>`;
    } else {
      return `<a href="${detailURL}${data.id}/">${data.title}</a>`;
    }
  }

  return exports;
};