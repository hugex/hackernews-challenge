'use strict';

/**
 * gets and parses data from the url
 * @param {string} baseURL base URL to the API - every entry point will be added to that url
 */
var ApiHandler = function(baseURL) {
  let exports = {};

  const ACCESS_POINTS = [
    'topstories',
    'newstories',
    'beststories'
  ];

  let itemIDs = [];
  let itemList;

  /**
   * fetch list of latest post IDs
   * topstories, newstories or beststories
   * @param  {Number} [accessPointIndex] index of json to fetch [topstories, newstories, beststories]
   * @return {Promise}                  loading of json data, will resolve with number of items
   */
  exports.getIDs = function(accessPointIndex = 0) {
    accessPointIndex = Math.max(0,Math.min(ACCESS_POINTS.length,parseInt(accessPointIndex,10)));

    return new Promise( (resolve, reject) => {

      fetch(`${baseURL}${ACCESS_POINTS[accessPointIndex]}.json`)
        .then(data => data.json())
        .then(data => {
          itemIDs = data;
          itemList = getItemData();
          resolve(itemIDs.length);
        })
        .catch(error => reject(error));
    });
  };

  /**
   * start loading the next item from the itemID list, which was loaded via getIDs
   * @return {Promise|undefined} loading of data, will resolve with specific DataEntryItem object or undefined, if data is invalid
   */
  exports.nextOverviewItem = function() {
    if(typeof itemList !== 'undefined' && typeof itemList.next !== 'undefined') {
      return itemList.next();
    } else {
      throw new Error('ID list needed, please call getIDs first');
    }
  };

  //generator function to load all items from the itemID list
  function* getItemData() {
    for(let i = 0; i < itemIDs.length; i++) {
      yield new Promise( (resolve, reject) => {

        fetch(`${baseURL}item/${itemIDs[i]}.json`)
          .then(data => data.json())
          .then(data => {
            //change data structure and add some needed methods
            //catch if null comes back
            if(data === null) {
              console.error('No valid data, data will be ignored', itemIDs[i]);
              resolve(undefined);
            } else if(data.type === 'story') {
              resolve(('url' in data) ? new StoryItem(data) : new AskItem(data));
            } else if(data.type === 'job') {
              resolve(new JobItem(data));
            } else if(data.type === 'poll') {
              resolve(new PollItem(data));
            } else {
              console.error('No valid data type, data will be ignored', data);
              resolve(undefined);
            }
            
          })
          .catch(error => reject(error));
      });
    }
  }

  /**
   * start loading a specifc item by id
   * @param  {Number} [id] id of item to fetch
   * @return {Promise} loading of data, will resolve with specific DataEntryItem object or throw Error, if data is invalid
   */
  exports.getItem = function(id) {
    return new Promise( (resolve, reject) => {

      fetch(`${baseURL}item/${id}.json`)
        .then(data => data.json())
        .then(data => {
          //change data structure and add some needed methods
          //catch if null comes back
          if(data === null) {
            throw new Error('No valid data');
          } else if(data.type === 'story') {
            resolve(('url' in data) ? new StoryItem(data) : new AskItem(data));
          } else if(data.type === 'job') {
            resolve(new JobItem(data));
          } else if(data.type === 'poll') {
            let item = new PollItem(data);
            //check if the poll has pollparts
            //and load them
            if(item.hasParts) {
              exports.getRelatedData(item.partsIDs)
                .then(values  => {
                  //add poll parts to poll data object and resolve
                  item.addParts(values);
                  resolve(item);
                })
                .catch(error => {
                  //if an error occurs, resolve without the parts
                  resolve(item);
                });
            } else {
              resolve(item);
            }

          } else {
            console.log(data);
            throw new Error('No valid data type');
          }

        })
        .catch(error => reject(error));
    });
  };

  /**
   * start loading a related items
   * @param  {Array} [relatedIDs] list of items to fetch
   * @return {Promise} loading of data, will resolve with CommentItem object, PollOption object or undefined, if data is invalid
   */
  exports.getRelatedData = function(relatedIDs) {
    return new Promise( (resolve, reject) => {
      //if the id list is empty, resolve with empty array
      if(relatedIDs.length < 1) resolve([]);

      //create multiple promises for current level and wait for resolve all
      let relatedPromises = [];

      for (let i = 0; i < relatedIDs.length; i++) {
        const id = relatedIDs[i];
        relatedPromises.push( getRelatedDataItem(id) );
      }
      
      Promise.all(relatedPromises)
        .then(values  => {
          resolve(values);
        })
        .catch(error => reject(error));
    });
  };

  //start loading a related item
  function getRelatedDataItem(id) {
    return new Promise( (resolve, reject) => {

      fetch(`${baseURL}item/${id}.json`)
        .then(data => data.json())
        .then(data => {
          //change data structure and add some needed methods
          //catch if null comes back
          if(data === null) {
            console.error('No valid data, data will be ignored', id);
            resolve(undefined);

          } else if(data.type === 'comment') {
            let commentData = new CommentItem(data);

            if(commentData.hasChildren) {
              //if comment has child comments
              //load recursive
              exports.getRelatedData(commentData.commentIDs)
                .then(data => {
                  commentData.addChildren(data);
                  resolve(commentData);
                })
                .catch(error => reject(error));
            } else {
              resolve(commentData);
            }

          } else if(data.type === 'pollopt') {

            resolve(new PollOption(data));

          } else {
            console.error('No valid data type, data will be ignored', data);
            resolve(undefined);
          }
        })
        .catch(error => reject(error));
    });
  }

  return exports;
}