/**
 * data of a ask item
 */
class AskItem extends DataEntryItem {
  constructor(data) {
    super(data);
    this.type = 'ask';

    this.commentCount = data.descendants;
    this.commentIDs = data.kids;

    this.text = data.text;
  }
}