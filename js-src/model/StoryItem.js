/**
 * data of a story item
 */
class StoryItem extends DataEntryItem {
  constructor(data) {
    super(data);
    this.type = 'story';

    this.commentCount = data.descendants;
    this.commentIDs = data.kids;

    this.url = data.url;
  }
}