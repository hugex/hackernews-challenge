/**
 * data of a job item
 */
class JobItem extends DataEntryItem {
  constructor(data) {
    super(data);
    this.type = 'job';

    this.url = data.url;
  }
}