/**
 * data of a poll item
 */
class PollItem extends DataEntryItem {
  constructor(data) {
    super(data);
    this.type = 'poll';

    this.commentCount = data.descendants;
    this.commentIDs = data.kids;

    this.partsIDs = data.parts;
    this.parts = [];

    this.text = data.text;
  }

  get highestPartsScore() {
    let highestScore = 0;

    for(let i = 0; i < this.parts.length; i++) {
      const part = this.parts[i];

      if(typeof part !== 'undefined' && part.type === 'pollopt') {
        if(part.score > highestScore) highestScore = part.score;
      }
    }

    return highestScore;
  }

  get hasParts() {
    return (typeof this.partsIDs !== 'undefined' && this.partsIDs.length > 0);
  }

  addPart(value) {
    if(typeof value !== 'undefined' && value.type === 'pollopt') {
      this.parts.push(value);
    }
  }

  addParts(value) {
    for(let i = 0; i < value.length; i++) {
      this.addPart(value[i]);
    }
  }
}