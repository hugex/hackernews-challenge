/**
 * data of a comment item
 */
class CommentItem extends DataItem {
  constructor(data) {
    super(data);
    this.type = 'comment';

    this.commentIDs = data.kids;
    this.commentChildren = [];
    this.commentChildCount = 0;

    this.parent = data.parent;

    this.text = data.text;
  }

  get hasChildren() {
    return (typeof this.commentIDs !== 'undefined' && this.commentIDs.length > 0);
  }

  addChild(value) {
    if(typeof value !== 'undefined' && value.type === 'comment') {
      this.commentChildren.push(value);

      this.commentChildCount++;
      this.commentChildCount += value.commentChildCount;
    }
  }

  addChildren(value) {
    for(let i = 0; i < value.length; i++) {
      this.addChild(value[i]);
    }
  }
}