/**
 * base class for data structure
 */
class DataItem {
  constructor(data) {
    this.id = data.id;
    this.type = 'dataItem';

    this.user = data.by;

    this.date = new Date(parseInt(data.time, 10) * 1000);

    this.visible = ('deleted' in data) ? !(data.deleted === true) : true;
  }

  get timeAgo() {
    const templates = {
        prefix: "",
        suffix: " ago",
        seconds: "less than a minute",
        minute: "about a minute",
        minutes: "%d minutes",
        hour: "about an hour",
        hours: "about %d hours",
        day: "a day",
        days: "%d days",
        month: "about a month",
        months: "%d months",
        year: "about a year",
        years: "%d years"
      };

    let template = function(t, n) {
      return templates[t] && templates[t].replace(/%d/i, Math.abs(Math.round(n)));
    };

    let now = new Date(),
        seconds = ((now.getTime() - this.date.getTime()) * .001) >> 0,
        minutes = seconds / 60,
        hours = minutes / 60,
        days = hours / 24,
        years = days / 365;

    return templates.prefix + (
            seconds < 45 && template('seconds', seconds) ||
            seconds < 90 && template('minute', 1) ||
            minutes < 45 && template('minutes', minutes) ||
            minutes < 90 && template('hour', 1) ||
            hours < 24 && template('hours', hours) ||
            hours < 42 && template('day', 1) ||
            days < 30 && template('days', days) ||
            days < 45 && template('month', 1) ||
            days < 365 && template('months', days / 30) ||
            years < 1.5 && template('year', 1) ||
            template('years', years)
            ) + templates.suffix;
  }
}