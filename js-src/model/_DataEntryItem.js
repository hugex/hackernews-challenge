/**
 * base class for data structure of entries (like story, poll, etc)
 */
class DataEntryItem extends DataItem {
  constructor(data) {
    super(data);

    this.score = parseInt(data.score, 10);

    this.title = data.title;
  }
}