/**
 * data of a poll part item
 */
class PollOption extends DataItem {
  constructor(data) {
    super(data);
    this.type = 'pollopt';

    this.text = data.text;

    this.score = parseInt(data.score, 10);
  }
}