module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        watch: {
            css: {
                files: ['scss/*.scss'],
                tasks: ['css'],
                options: {
                    interrupt: true
                }
            },
            js: {
                files: ['js-src/vendor/**/*.js', 'js-src/**/*.js', 'js-src/*.js'],
                tasks: ['js'],
                options: {
                    interrupt: true
                }
            }
        },

        compass: {
            desktop: {
                options: {
                    sassDir: 'scss',
                    cssDir: 'web/css',
                    outputStyle: 'compressed'
                }
            }
        },

        cssmin: {
            target: {
                files: [{
                    expand:true,
                    cwd: 'web/css',
                    src: ['*.css', '!*.min.css'],
                    dest: 'web/css',
                    ext: '.min.css'
                }]
            }
        },

        concat: {
            options: { separator: ';' },
            all: {
                src: [
                    'js-src/polyfill/*.js',
                    'js-src/**/*.js',
                    'js-src/*.js',
                    '!js-src/App.js',
                    'js-src/App.js'
                ],
                dest: 'web/js/scripts.js'
            }
        }
        
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');

    grunt.registerTask('css', ['compass']);
    grunt.registerTask('js', ['concat']);
    grunt.registerTask('default', ['watch']);
};
