# README #

Code challenge to build my own version of Hacker News

### Setup ###

Base setup for routing and layout is served va **Silex**. Use **composer** to setup.

JavaScript and CSS are generated via **grunt**. Use **npm install** and **grunt js** / **grunt css** to generate.

### toDO ###

* additional testing
* optional: switch between topstories, newstories, beststories - at the moment only topstories will be loaded