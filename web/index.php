<?php

// web/index.php
require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();

//add debug for now
$app['debug'] = true;

//register twig
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../templates',
));

//regster asset service provider
$app->register(new Silex\Provider\AssetServiceProvider(), array(
    'assets.version' => 'v1',
    'assets.version_format' => '%s?version=%s',
    'assets.named_packages' => array(
        'css' => array('base_path' => '/css'),
        'js' => array('base_path' => '/js'),
    ),
));

//------------------- ROUTE DEFINITIONS --------------
//home
$app->get('/', function () use ($app) {
  return $app['twig']->render('home.twig', array('id' => 0));
})->bind('homepage');

//detail
$app->get('/view/{id}', function ($id) use ($app) {
  return $app['twig']->render('detail.twig', array('id' => $id));
})
->assert('id', '\d+')
->bind('detailPage');

//default path for 404 - to send occuring 404 errors via javascript to
$app->get('/notFound/', function () use ($app) {
  return $app['twig']->render('Exception/error.404.twig', array());
})
->bind('notFoundPage');

//custom error pages
$app->error(function (\Exception $e) use ($app) {
  
  if ($e instanceof Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
    return $app['twig']->render('Exception/error.404.twig', array());
  }

  $code = ($e instanceof Symfony\Component\HttpKernel\Exception\HttpException) ? $e->getStatusCode() : 500;
  return $app['twig']->render('Exception/error.twig', array(
    'code' => $code,
  ));
});

//run app
$app->run();